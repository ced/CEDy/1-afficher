train = [14.62, 9.26]

en_tete = """Calculette : Eco-déplacements.
Calculez l'impact de vos déplacements quotidiens
sur l'environnement et vos dépenses"""
en_tete_cout = ["kg eq. CO2", "l eq. pétrole"]
ligne = "#" * 50
km = 10
domicile = "J'habite à {} km de mon travail.".format(km)

print(ligne)
print(en_tete)
print(ligne, end="\n"*2)
print(domicile, end="\n"*2)
print("Coût annuel pour un déplacement en train : ")
for libelle in en_tete_cout:
    print("\t", libelle, end="")
print()
for cout in train:
    print("\t", round(cout*km, 1), end="\t")
print()
