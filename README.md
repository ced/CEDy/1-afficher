Bien que la calculette de l'Ademe présente une interface graphique (GUI : Graphic User Interface), nous allons dans un premier temps, nous concentrer sur un programme en ligne de commandes (CLI : Command Line Interface).
**Écrire un programme CLI**, par rapport à un un programme GUI, présente de nombreux avantages, ne serait-ce que l'économie de moyens nécessaires au fonctionnement.
Un programme CLI sera surtout **beaucoup plus facile à écrire** dans un premier temps car nous n'aurons pas à interagir avec le serveur graphique et le gestionnaire de fenêtres et autres composants graphiques.

> **Notre mission**, dans un premier temps, aura pour objectif d'afficher l'en tête résumant le programme : « Calculette : Eco-déplacements. Calculez l'impact de vos déplacements quotidiens sur l'environnement et vos dépenses. ».

> Puis nous essayerons d'afficher la distance entre le domicile et le travail. Enfin, nous afficherons les coûts (financier, effet de serre, énergétique) des différents modes de transports.
